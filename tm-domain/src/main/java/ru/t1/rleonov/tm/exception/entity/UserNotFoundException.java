package ru.t1.rleonov.tm.exception.entity;

public final class UserNotFoundException extends AbstractEntityNotFoundException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
