package ru.t1.rleonov.tm.endpoint;

import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.marker.IntegrationCategory;

@Category(IntegrationCategory.class)
public final class UserEndpointTest {

    /*@NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private static String user1Token;

    @Nullable
    private static String user2Token;

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUpClass() {
        adminToken = login(ADMIN);
        TestUtil.reloadData(adminToken);
    }

    @AfterClass
    public static void resetClass() {
        adminToken = login(ADMIN);
        TestUtil.reloadData(adminToken);
    }

    @Before
    public void setUp() {
        user1Token = login(USER1);
        user2Token = login(USER2);
        adminToken = login(ADMIN);
    }

    @After
    public void reset() {
        TestUtil.logout(user1Token);
        TestUtil.logout(user2Token);
        TestUtil.logout(adminToken);
    }

    @Test
    public void userChangePasswordProfile() {
        @NotNull final UserChangePasswordProfileRequest request = new UserChangePasswordProfileRequest(user1Token);
        request.setPassword(USER1.getPassword());
        Assert.assertNotNull(userEndpoint.changePassword(request));
    }

    @Test
    public void userLockUnlock() {
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken);
        unlockRequest.setLogin(USER1.getLogin());
        Assert.assertNotNull(userEndpoint.unlockUser(unlockRequest));
        @NotNull final UserLockRequest request = new UserLockRequest(adminToken);
        request.setLogin(USER1.getLogin());
        Assert.assertNotNull(userEndpoint.lockUser(request));
        Assert.assertThrows(Exception.class, () -> login(USER1));
        Assert.assertNotNull(userEndpoint.unlockUser(unlockRequest));
        user1Token = login(USER1);
    }

    @Test
    public void userRegistry() {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(NEW_USER.getLogin());
        request.setPassword(NEW_USER.getPassword());
        request.setEmail(NEW_USER.getEmail());
        @Nullable final User user = userEndpoint.registryUser(request).getUser();
        Assert.assertEquals(NEW_USER.getLogin(), user.getLogin());
        @Nullable final String newUserToken = TestUtil.login(NEW_USER);
        TestUtil.logout(newUserToken);
    }

    @Test
    public void userRemove() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER1.getLogin());
        @Nullable final User user = userEndpoint.removeUser(request).getUser();
        Assert.assertEquals(USER1.getLogin(), user.getLogin());
        Assert.assertThrows(Exception.class, () -> login(USER1));
        resetClass();
    }

    @Test
    public void userUpdateProfile() {
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(user1Token);
        request.setLastName(USER1.getLogin());
        request.setFirstName(USER1.getLogin());
        request.setMiddleName(USER1.getLogin());
        @Nullable final User user = userEndpoint.updateUserProfile(request).getUser();
        Assert.assertEquals(USER1.getLogin(), user.getLastName());
    }

    @Test
    public void userViewProfileRequest() {
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(user1Token);
        @Nullable final User user = userEndpoint.viewUserProfile(request).getUser();
        Assert.assertEquals(USER1.getLogin(), user.getLogin());
    }*/

}
