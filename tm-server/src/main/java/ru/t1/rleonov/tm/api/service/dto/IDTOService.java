package ru.t1.rleonov.tm.api.service.dto;

import ru.t1.rleonov.tm.api.repository.dto.IDTORepository;
import ru.t1.rleonov.tm.dto.model.AbstractModelDTO;

public interface IDTOService<M extends AbstractModelDTO> extends IDTORepository<M> {
}
