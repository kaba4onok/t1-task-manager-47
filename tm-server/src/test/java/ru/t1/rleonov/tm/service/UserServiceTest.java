package ru.t1.rleonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.model.IProjectRepository;
import ru.t1.rleonov.tm.api.repository.model.ITaskRepository;
import ru.t1.rleonov.tm.api.repository.model.IUserRepository;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.api.service.model.IUserService;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.repository.model.ProjectRepository;
import ru.t1.rleonov.tm.repository.model.TaskRepository;
import ru.t1.rleonov.tm.repository.model.UserRepository;
import ru.t1.rleonov.tm.service.model.UserService;
import javax.persistence.EntityManager;


@Category(UnitCategory.class)
public final class UserServiceTest {
/*
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    private final IUserRepository userRepository = new UserRepository(entityManager);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(entityManager);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository(entityManager);

    @NotNull
    private final IUserService service = new UserService(connectionService, propertyService);

    @Test
    public void removeByLogin() {
        service.removeByLogin("user2");
    }
*/
}
